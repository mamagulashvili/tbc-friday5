package com.example.tbcblackfriday5

import android.content.Context
import android.text.Editable
import android.text.InputType.TYPE_CLASS_NUMBER
import android.text.InputType.TYPE_CLASS_TEXT
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.tbcblackfriday5.databinding.RowChooserItemBinding
import com.example.tbcblackfriday5.databinding.RowEditTextItemBinding

class ItemsAdapter(
    val itemList: MutableList<ItemModel>,val context: Context
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val CHOOSER_ITEM_TYPE = 0
        private const val INPUT_ITEM_TYPE = 1

        val itemMap = mutableMapOf<Int,String>()
    }
    lateinit var item: ItemModel

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == CHOOSER_ITEM_TYPE) {
            ChooserItemViewHolder(
                RowChooserItemBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )

        } else {
            InputItemViewHolder(
                RowEditTextItemBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (itemList[position].fieldType) {
            "chooser" -> CHOOSER_ITEM_TYPE
            else -> INPUT_ITEM_TYPE
        }
    }

    override fun getItemCount(): Int = itemList.size


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ChooserItemViewHolder -> holder.onBind()
            is InputItemViewHolder -> holder.onBind()
        }
    }

    inner class InputItemViewHolder(val binding: RowEditTextItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun onBind() {
            item = itemList[bindingAdapterPosition]
            binding.apply {
                editText.hint = item.hint
                if (item.keyboard != null) {
                    when (item.keyboard) {
                        "text" -> editText.inputType = TYPE_CLASS_TEXT
                        "number" -> editText.inputType = TYPE_CLASS_NUMBER
                    }
                }
            }
        }
    }

    inner class ChooserItemViewHolder(val binding: RowChooserItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind() {
            item = itemList[bindingAdapterPosition]
            when(item.hint){
                "Gender" -> {
                    binding.textInputLayout.visibility = View.VISIBLE
                    val resource = context.resources.getStringArray(R.array.gender)
                    val arrayAdapter = ArrayAdapter(context,R.layout.drop_down_item,resource)
                    binding.dropdownMenu.setAdapter(arrayAdapter)
                    binding.dropdownMenu.hint = item.hint

                }
                "Birthday" -> {
                    binding.datePicker.visibility = View.VISIBLE
                }
            }
        }
    }
}