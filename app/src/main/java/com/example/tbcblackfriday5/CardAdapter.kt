package com.example.tbcblackfriday5

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.tbcblackfriday5.databinding.RowCardItemBinding

class CardAdapter(val itemAmount: Int, val itemList: MutableList<MutableList<ItemModel>>,val context: Context) :
    RecyclerView.Adapter<CardAdapter.CardViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewHolder {
        return CardViewHolder(
            RowCardItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: CardViewHolder, position: Int) {
        holder.onBind()
        setChildRecycle(holder.binding.rvItem, itemList[position])
    }

    override fun getItemCount(): Int = itemAmount

    inner class CardViewHolder(val binding: RowCardItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind() {
        }

    }

    fun setChildRecycle(
        recyclerView: RecyclerView,
        itemList: MutableList<ItemModel>
    ) {
        val itemRecycleAdapter = ItemsAdapter(itemList,context)
        recyclerView.apply {
            layoutManager = LinearLayoutManager(recyclerView.context)
            adapter = itemRecycleAdapter
        }
    }
}