package com.example.tbcblackfriday5

import android.util.Log
import android.util.Log.d
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.json.JSONArray
import org.json.JSONException

class MainViewModel : ViewModel() {
    init {
        viewModelScope.launch {
            withContext(Dispatchers.Default) {
                getData()
            }
        }
    }

    private val itemList = MutableLiveData<MutableList<MutableList<ItemModel>>>()
    val _itemList: LiveData<MutableList<MutableList<ItemModel>>> = itemList

    val jsonList = mutableListOf<MutableList<ItemModel>>()
    suspend fun getData() = viewModelScope.launch {
        try {
            val data = JSONArray(json)
            for (i in 0 until data.length()) {
                val lists = data.getJSONArray(i)
                jsonList.add(mutableListOf<ItemModel>())
                for (j in 0 until lists.length()) {
                    val jsonObject = lists.getJSONObject(j)
                    jsonList[i].add(
                        ItemModel(
                            jsonObject.getInt("field_id"),
                            jsonObject.getString("hint"),
                            jsonObject.getString("field_type"),
                            jsonObject.getBoolean("required"),
                            jsonObject.getBoolean("is_active"),
                            jsonObject.getString("icon"),
                            if (jsonObject.has("keyboard")) {
                                jsonObject.getString("keyboard")
                            } else {
                                null
                            }
                        )
                    )
                }
            }
            itemList.postValue(jsonList)
            d("JSON", "$jsonList")


        } catch (e: JSONException) {
            Log.d("ERROR", "$e")
        }
    }

    val json = "[\n" +
            "   [\n" +
            "      {\n" +
            "         \"field_id\":1,\n" +
            "         \"hint\":\"UserName\",\n" +
            "         \"field_type\":\"input\",\n" +
            "         \"keyboard\":\"text\",\n" +
            "         \"required\":false,\n" +
            "         \"is_active\":true,\n" +
            "         \"icon\":\"https://jemala.png\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"field_id\":2,\n" +
            "         \"hint\":\"Email\",\n" +
            "         \"field_type\":\"input\",\n" +
            "         \"required\":true,\n" +
            "         \"keyboard\":\"text\",\n" +
            "         \"is_active\":true,\n" +
            "         \"icon\":\"https://jemala.png\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"field_id\":3,\n" +
            "         \"hint\":\"phone\",\n" +
            "         \"field_type\":\"input\",\n" +
            "         \"required\":true,\n" +
            "         \"keyboard\":\"number\",\n" +
            "         \"is_active\":true,\n" +
            "         \"icon\":\"https://jemala.png\"\n" +
            "      }\n" +
            "   ],\n" +
            "   [\n" +
            "      {\n" +
            "         \"field_id\":4,\n" +
            "         \"hint\":\"Full Name\",\n" +
            "         \"field_type\":\"input\",\n" +
            "         \"keyboard\":\"text\",\n" +
            "         \"required\":true,\n" +
            "         \"is_active\":true,\n" +
            "         \"icon\":\"https://jemala.png\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"field_id\":14,\n" +
            "         \"hint\":\"Jemali\",\n" +
            "         \"field_type\":\"input\",\n" +
            "         \"keyboard\":\"text\",\n" +
            "         \"required\":false,\n" +
            "         \"is_active\":true,\n" +
            "         \"icon\":\"https://jemala.png\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"field_id\":89,\n" +
            "         \"hint\":\"Birthday\",\n" +
            "         \"field_type\":\"chooser\",\n" +
            "         \"required\":false,\n" +
            "         \"is_active\":true,\n" +
            "         \"icon\":\"https://jemala.png\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"field_id\":898,\n" +
            "         \"hint\":\"Gender\",\n" +
            "         \"field_type\":\"chooser\",\n" +
            "         \"required\":\"false\",\n" +
            "         \"is_active\":true,\n" +
            "         \"icon\":\"https://jemala.png\"\n" +
            "      }\n" +
            "   ]\n" +
            "]"

}