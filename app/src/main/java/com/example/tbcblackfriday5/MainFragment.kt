package com.example.tbcblackfriday5

import android.os.Bundle
import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.core.view.children
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.tbcblackfriday5.databinding.MainFragmentBinding

class MainFragment : Fragment() {


    private val viewModel: MainViewModel by viewModels()
    private lateinit var binding: MainFragmentBinding
    private lateinit var cardAdapter: CardAdapter
    private val itemList = mutableListOf<MutableList<ItemModel>>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = MainFragmentBinding.inflate(layoutInflater, container, false)
        init()
        return binding.root
    }

    private fun init() {
        observer()
        binding.fabRegistration.setOnClickListener {
            registration()
        }
    }

    private fun observer() {
        viewModel._itemList.observe(viewLifecycleOwner, {
            setRecycle(it.size, it)
            itemList.addAll(it)
        })
    }

    private fun registration() {
        val itemMap = mutableMapOf<Int, String?>()
        binding.firstRv.children.forEach { recycle ->
            recycle.findViewById<RecyclerView>(R.id.rvItem).children.forEach {
                val editText: EditText? = it.findViewById<EditText>(R.id.editText)
                for (i in 0 until itemList.size) {
                    val subList = itemList[i]
                    for (j in 0 until subList.size) {
                        if (subList[j].hint == editText?.hint) {
                            itemMap[subList[j].fieldId] = editText.text.toString()
                        }
                    }
                }
            }
        }

        for (i in 0 until itemList.size) {
            val sublist = itemList[i]
            for (j in 0 until sublist.size) {
                itemMap.forEach {
                    if (sublist[j].required && it.key == sublist[j].fieldId && it.value == "") {
                        Toast.makeText(
                            binding.firstRv.context,
                            "Please Fill ${sublist[j].hint} field",
                            Toast.LENGTH_SHORT
                        ).show()
                    }else{
                        Toast.makeText(binding.firstRv.context, "Successfully", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
        itemMap.clear()
    }

    private fun setRecycle(amount: Int, itemList: MutableList<MutableList<ItemModel>>) {
        cardAdapter = CardAdapter(amount, itemList, requireContext())
        binding.firstRv.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = cardAdapter
        }
    }
}